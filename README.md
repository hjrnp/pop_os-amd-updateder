# Pop_OS AMD updateder

A short script to update Pop_OS and ensure/retain use of the amdgpu proprietary video drivers (the ones that use Vulkan). 


##What this script does:

    - script updates your Pop_OS

    - Checks AMD graphics drivers (script may BREAK nVida/intel systems, it's not meant for them!)

    - Updates system to use Vulkan graphics driver for AMD (Much better gaming!)

    - Gets rid of startup splash screen and gives text output so if something every breaks on your system (such as from an update), you can have an easier time troubleshooting.

So, several times I've updated Pop_OS! and it either broke on updates (because the updater PopShop called required user interaction that wasn't available) , or set radeon as the default graphic driver instead of amdgpu (the latter being the one that includes Vulkan for gaming; I can play most games with the amdgpu driver, and very few with the radeon driver, so if you're having problems playing games, this will also be very helpful).

I've created this script to address that. Take the following script down below, and copy it to your ~/bin/ folder, and name it

~~~bash
    lil_update.sh
~~~

and then, with the terminal, do the following:

~~~bash
cd ~/bin/

chmod 755 lil_update.sh
~~~

Then, when you want to update and/or just make sure you're running Vulkan drivers, just go to your terminal, and type in "lil_update.sh"
