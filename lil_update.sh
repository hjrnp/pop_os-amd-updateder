
#!/bin/bash
if (($EUID != 0)); then
  if [[ -t 1 ]]; then
    sudo "$0" "$@"
  else
    exec 1>output_file
    gksu "$0 $@"
  fi
  exit
fi

apt update

apt list --upgradable

read -p "Do you wish to continue?" -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]; then
    apt upgrade
else
    exit 1
fi

lspci -k | grep -EA3 'VGA|3D|Display' | grep "Kernel driver in use: amdgpu"

tail /boot/efi/loader/entries/Pop_OS-current.conf | grep "amdgpu.si_support=1 radeon.si_support=0 amdgpu.cik_support=1 radeon.cik_support=0"


if lspci -k | grep -EA3 'VGA|3D|Display' | grep -q "Kernel driver in use: amdgpu"; then
    printf "AMDGPU currently in use, "
fi

if tail /boot/efi/loader/entries/Pop_OS-current.conf | grep -q "amdgpu.si_support=1 radeon.si_support=0 amdgpu.cik_support=1 radeon.cik_support=0"; then
	echo "and AMDGPU will be kept on reset."
else
    echo "but will not be kept on reset."
    echo "Please attempt similar to the following..."
    echo "sed -i 's/systemd.show_status=false splash/amdgpu.si_support=1 radeon.si_support=0 amdgpu.cik_support=1 radeon.cik_support=0/g' /boot/efi/loader/entries/Pop_OS-current.conf"
    echo "The expected result would be..."
        echo "==========/boot/efi/loader/entries/Pop_OS-current.conf================"
    sed -e 's/systemd.show_status=false splash/amdgpu.si_support=1 radeon.si_support=0 amdgpu.cik_support=1 radeon.cik_support=0/g' /boot/efi/loader/entries/Pop_OS-current.conf
    echo "=========================================================================="
    
    read -p "Shall I attempt to do it automatically?" -n 1 -r

    if [[ $REPLY =~ ^[Yy]$ ]]; then
	sed -i 's/systemd.show_status=false splash/amdgpu.si_support=1 radeon.si_support=0 amdgpu.cik_support=1 radeon.cik_support=0/g' /boot/efi/loader/entries/Pop_OS-current.conf
    else
    	exit 1
    fi
    
fi
